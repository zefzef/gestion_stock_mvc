package com.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class MtvtStik implements Serializable {
	public static final int ENTREE=1;
	
	public static final int SSORTIE=2;
	
	@Id
	@GeneratedValue
private Long idMtvtStik;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date deteMVT;
	
	private BigDecimal quantite;
	
	private int  typeMVt;
	
	@ManyToOne
    @JoinColumn(name="idArticle")
	private Article article;
	
	public Long getIdMtvtStik() {
		return idMtvtStik;
	}

	public void setIdMtvtStik(Long id) {
		this.idMtvtStik = id;
}

	public Date getDeteMVT() {
		return deteMVT;
	}

	public void setDeteMVT(Date deteMVT) {
		this.deteMVT = deteMVT;
	}

	public BigDecimal getQuantite() {
		return quantite;
	}

	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}

	public int getTypeMVt() {
		return typeMVt;
	}

	public void setTypeMVt(int typeMVt) {
		this.typeMVt = typeMVt;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	

}
