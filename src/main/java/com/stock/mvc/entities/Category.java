package com.stock.mvc.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Category implements Serializable{

	
	@Id
	@GeneratedValue
private Long idCactory;
	private String code;
	private String designation;
	
	@OneToMany(mappedBy="category")
	private List<Article> Articles;
	
	 public Category() {}
	
		public Long getIdFactory() {
		return idCactory;
	}

	public void setIdFactory(Long idFactory) {
		this.idCactory = idFactory;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public List<Article> getArticles() {
		return Articles;
	}

	public void setArticles(List<Article> articles) {
		Articles = articles;
	}


	public Long getIdCactory() {
		return idCactory;
	}


	public void setIdCactory(Long idCactory) {
		this.idCactory = idCactory;
	}
	
}
